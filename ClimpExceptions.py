# Color Exceptions
class InvalidColorError(Exception):
    """
    Excepton if invalid color is passed

    param _value (str): Value of color passed
    """

    def __init__(this, _value):
        super().__init__("Failed to parse color " + str(_value))


# Image Exceptions
class UnsavedFileError(Exception):
    """
    Excepton if unsaved file and attempting to create/open another
    """


# Transformation Exceptions
class InvalidPropertyError(Exception):
    """
    Exception if invalid property changed

    param _prop (str): Property name
    """

    def __init__(this, _prop, _type):
        super().__init__("Invalid property " + str(_prop) + " on " + str(_type))


class InvalidFormat(Exception):
    """
    Exception if invalid format for changing property

    param _arg (str): Argument passed
    """

    def __init__(this, _arg):
        super().__init__("Invalid format " + str(_arg))


class InvalidArguments(Exception):
    """
    Exception if invalid arguments passed

    param _arg (str): Argument passed
    """

    def __init__(this, _arg):
        super().__init__("Invalid arguments passed " + str(_arg))


class PropertyNotFound(Exception):
    """
    Exception raised f specified enum value is not found
    """

    def __init__(this, _msg):
        super().__init__(_msg)


# History Exceptions


class HistoryError(Exception):
    """
    Exception raised if unable to move in history

    param _msg (str) Message to show
    """

    def __init__(this, _msg):
        super().__init__(_msg)
