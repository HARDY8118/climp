from abc import ABC, abstractmethod
from re import findall
import cv2

from ClimpExceptions import (
    InvalidColorError,
    InvalidPropertyError,
    InvalidFormat,
    PropertyNotFound,
)


def parse_color(_value):
    """
    Parse value as color and return (rgb) tuple

    param _value (str|tuple): Color value to be parsed

    Returns: Parsed color as (r,g,b) tuple

    Raises: InvalidColorError
    """
    __presets = {
        "black": (
            0,
            0,
            0,
        ),
        "blue": (
            255,
            0,
            0,
        ),
        "cyan": (
            255,
            255,
            0,
        ),
        "green": (
            0,
            255,
            0,
        ),
        "grey": (
            128,
            128,
            128,
        ),
        "red": (
            0,
            0,
            255,
        ),
        "pink": (
            255,
            0,
            255,
        ),
        "white": (
            255,
            255,
            255,
        ),
        "yellow": (
            0,
            255,
            255,
        ),
    }
    if type(_value) == str:
        if _value.lower() in __presets:
            return __presets[_value.lower()]
        elif len(_value) in (4, 7):
            if _value[0] == "#":
                try:
                    if len(_value) == 4:
                        return (
                            int(_value[1], 16),
                            int(_value[2], 16),
                            int(_value[3], 16),
                        )
                    if len(_value) == 7:
                        return (
                            int(_value[1:3], 16),
                            int(_value[3:5], 16),
                            int(_value[5:7], 16),
                        )
                except ValueError:
                    raise InvalidColorError(_value)
            else:
                raise InvalidColorError(_value)
        else:
            raise InvalidColorError(_value)

    elif type(_value) == tuple and len(_value) == 3:
        if 0 <= _value[0] < 256 and 0 <= _value[1] < 256 and 0 <= _value[2] < 256:
            return _value[::-1]
        else:
            raise InvalidColorError(_value)
    else:
        raise InvalidColorError(_value)


def getFontFromName(_font):
    _fonts = {
        "FONT_HERSHEY_SIMPLEX": cv2.FONT_HERSHEY_SIMPLEX,
        "HERSHEY_SIMPLEX": cv2.FONT_HERSHEY_SIMPLEX,
        "SIMPLEX": cv2.FONT_HERSHEY_SIMPLEX,
        "FONT_HERSHEY_PLAIN": cv2.FONT_HERSHEY_PLAIN,
        "HERSHEY_PLAIN": cv2.FONT_HERSHEY_PLAIN,
        "PLAIN": cv2.FONT_HERSHEY_PLAIN,
        "FONT_HERSHEY_DUPLEX": cv2.FONT_HERSHEY_DUPLEX,
        "HERSHEY_DUPLEX": cv2.FONT_HERSHEY_DUPLEX,
        "DUPLEX": cv2.FONT_HERSHEY_DUPLEX,
        "FONT_HERSHEY_COMPLEX": cv2.FONT_HERSHEY_COMPLEX,
        "HERSHEY_COMPLEX": cv2.FONT_HERSHEY_COMPLEX,
        "COMPLEX": cv2.FONT_HERSHEY_COMPLEX,
        "FONT_HERSHEY_TRIPLEX": cv2.FONT_HERSHEY_TRIPLEX,
        "HERSHEY_TRIPLEX": cv2.FONT_HERSHEY_TRIPLEX,
        "TRIPLEX": cv2.FONT_HERSHEY_TRIPLEX,
        "FONT_HERSHEY_COMPLEX_SMALL": cv2.FONT_HERSHEY_COMPLEX_SMALL,
        "HERSHEY_COMPLEX_SMALL": cv2.FONT_HERSHEY_COMPLEX_SMALL,
        "COMPLEX_SMALL": cv2.FONT_HERSHEY_COMPLEX_SMALL,
        "FONT_HERSHEY_SCRIPT_SIMPLEX": cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
        "HERSHEY_SCRIPT_SIMPLEX": cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
        "SCRIPT_SIMPLEX": cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
        "FONT_HERSHEY_SCRIPT_COMPLEX": cv2.FONT_HERSHEY_SCRIPT_COMPLEX,
        "HERSHEY_SCRIPT_COMPLEX": cv2.FONT_HERSHEY_SCRIPT_COMPLEX,
        "SCRIPT_COMPLEX": cv2.FONT_HERSHEY_SCRIPT_COMPLEX,
        # "FONT_ITALIC": cv2.FONT_ITALIC,
        # "ITALIC": cv2.FONT_ITALIC,
    }

    if type(_font) == str and _font.upper() in _fonts:
        return _fonts[_font.upper()]
    elif type(_font) == int:
        return _font
    raise PropertyNotFound("Invalid font name " + _font)


def getLineType(_line):
    if _line == "-1" or _line.upper() == "FILLED" or _line.upper() == "FILL":
        return cv2.FILLED
    elif _line == "4" or _line.upper() == "LINE_4":
        return cv2.LINE_4
    elif _line == "8" or _line.upper() == "LINE_8":
        return cv2.LINE_8
    elif (
        _line == "16"
        or _line.upper() == "AA"
        or _line.upper() == "LINE_16"
        or _line.upper() == "LINE_AA"
    ):
        return cv2.LINE_AA
    else:
        raise PropertyNotFound("Invalid line type " + _line)


def parse_changes(_args):
    _changes = findall(r"[a-zA-Z0-9]+=[a-zA-Z0-9\s]+;", _args)
    if _changes is None:
        raise InvalidFormat(_args)
    else:
        _props = []
        for _change in _changes:
            _prop, _val = findall(r"[a-zA-Z]+=|[a-zA-Z0-9\s]+;", _change)
            _prop = _prop[:-1].lower()
            _val = _val[:-1].lower()
            if _prop.lower() == "fontface":
                _val = getFontFromName(_val)
            elif _prop.lower() == "linetype":
                _val = getLineType(_val)
            elif _prop.lower() == "color":
                _val = parse_color(_val)

            _props.append(
                (
                    _prop,
                    _val,
                )
            )
        return _props


class Transformation(ABC):
    """
    Transformation properties base cass
    """

    def __init__(this, x=0, y=0, color=(0, 0, 0)):
        """
        Initialize x,y for transformation
        """
        this.x = int(x)
        this.y = int(y)
        this.color = parse_color(color)

    @abstractmethod
    def apply(this):
        """
        Apply transformation
        """
        pass

    def move(this, _args):
        """
        Change x and y values

        param _args (str) Movement values
        """
        _changes = findall(r"[xy]2?[+-=]\d+", _args)
        if _changes is None:
            print("Invalid format")
        else:
            for _change in _changes:
                _prop, _v = findall(r"[a-z]2?|[+-=]\d+", _change)
                if hasattr(this, _prop):
                    if _v[0] == "+":
                        setattr(this, _prop, getattr(this, _prop) + int(_v))
                    elif _v[0] == "-":
                        setattr(this, _prop, getattr(this, _prop) + int(_v))
                    elif _v[0] == "=":
                        setattr(this, _prop, int(_v))
                else:
                    raise InvalidPropertyError(_prop, type(this))

    def modify(this, _args):
        """
        Modify text transformation properties

        param _args list of list having property and value
        """
        for _p, _v in _args:
            if hasattr(this, _p):

                setattr(this, _p, _v)
            else:
                raise InvalidPropertyError(_p, type(this))


class LineTransform(Transformation):
    """
    Add line to image
    """

    def __init__(this, x, y, x2, y2, color=(0, 0, 0), thickness=1, lineType="8"):
        """
        Initialize line transformation properties

        raises InvalidColorError, ValueError, PropertyNotFound
        """
        super().__init__(x, y, color)
        this.x2 = int(x2)
        this.y2 = int(y2)
        this.thickness = int(thickness)
        this.linetype = getLineType(lineType)

    def apply(this, _base):
        """
        Apply line transformation to image

        param _base Base image to apply transformation
        """
        cv2.line(
            _base,
            (
                this.x,
                this.y,
            ),
            (
                this.x2,
                this.y2,
            ),
            this.color,
            this.thickness,
            this.linetype,
        )


class RectTransform(Transformation):
    """
    Add rectangle to image
    """

    def __init__(this, x, y, x2, y2, color=(0, 0, 0), thickness=-1, lineType="8"):
        """
        Initialize rectangle transformation properties

        raises InvalidColorError, ValueError, PropertyNotFound
        """
        super().__init__(x, y, color)
        this.x2 = x2
        this.y2 = y2
        this.color = parse_color(color)
        this.thickness = int(thickness)
        this.linetype = getLineType(lineType)

    def apply(this, _base):
        """
        Apply rectangle transformation to image

        param _base Base image to apply transformation
        """
        cv2.rectangle(
            _base,
            (this.x, this.y),
            (this.x2, this.y2),
            this.color,
            this.thickness,
            this.linetype,
        )


class CircleTransform(Transformation):
    """
    Add circle to image
    """

    def __init__(this, x, y, radius=1, color=(0, 0, 0), thickness=-1):
        """
        Initialize circle transformation properties
        """
        super().__init__(x, y, color)
        this.radius = radius
        this.thickness = int(thickness)

    def apply(this, _base):
        """
        Apply circle transformation to image

        param _base Base image to apply transformation
        """
        cv2.circle(_base, (this.x, this.y), this.radius, this.color, this.thickness)


class EllipseTransform(Transformation):
    """
    Add ellipse to image
    """

    def __init__(this, x, y):
        """
        Initialize ellipse transformation image
        """
        super(x, y)

    def apply(this):
        pass


class PolygonTransform(Transformation):
    """
    Add polygon to image
    """

    def __init__(this, x, y):
        """
        Initialize polygon transformation image
        """
        super(x, y)

    def apply(this):
        pass


class TextTransform(Transformation):
    """
    Add text to image
    """

    def __init__(
        this,
        text="",
        x=0,
        y=0,
        fontFace="plain",
        fontScale=1.0,
        color=(0, 0, 0),
        thickness=1,
        lineType="-1",
    ):
        """
        Initialize text transformation properties

        raises InvalidColorError, ValueError, PropertyNotFound
        """
        super().__init__(x, y, color)
        this.text = str(text)
        this.fontface = getFontFromName(fontFace)
        this.fontscale = float(fontScale)
        this.thickness = int(thickness)
        this.linetype = getLineType(lineType)

    def apply(this, _base):
        """
        Apply text transformation to image

        param _base Base image to apply transformation
        """
        cv2.putText(
            _base,
            this.text,
            (
                this.x,
                this.y,
            ),
            this.fontface,
            this.fontscale,
            this.color,
            this.thickness,
            this.linetype,
        )
