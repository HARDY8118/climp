#!/usr/bin/python

from argparse import ArgumentParser
from pathlib import Path
from os import environ
import sys
import yaml


class ConfigObject:
    """
    Custom config object class
    """

    def __init__(this):
        """
        Return configs as dict
        """
        parser = ArgumentParser()

        parser.add_argument("file", nargs='?', default=False, help="Name or path to image file")
        parser.add_argument("--configfile", help="Path to config file")
        parser.add_argument(
            "-v", "--verbose", help="Show debug messages on action", action="store_true"
        )
        parser.add_argument(
            "-p",
            "--preview",
            help="Show image after each transformation",
            action="store_true",
        )

        args = parser.parse_args()

        # FIle
        if args.file:
            this.file = args.file
        else:
            this.file = False

        # Config
        if args.configfile:
            _configPath = Path().cwd()
            _configPath /= args.configfile
        else:
            _configPath = Path(environ["HOME"])
            _configPath /= ".climpConfig"
            if _configPath.exists() and _configPath.is_file():
                this._loadConfigs(_configPath)
            else:
                _f = open(_configPath.resolve(), "w")
                _f.write("verbose: true\npreview: true\n")
                _f.close()
                this._loadConfigs(_configPath)

        # Verbose
        if args.verbose:
            this.verbose = True

        # Preview
        if args.preview:
            this.preview = True

    def _loadConfigs(this, _path):
        """
        Load configs from file
        """
        if _path.exists() and _path.is_file():
            this.configfile = _path.resolve().__str__()
            with open(_path.resolve(), "r") as _cf:
                _configData = yaml.safe_load(_cf)
                if "verbose" in _configData:
                    this.verbose = _configData["verbose"]
                else:
                    this.verbose = False

                if "preview" in _configData:
                    this.preview = _configData["preview"]
                else:
                    this.preview = False
        else:
            print("Invalid config file")
            sys.exit(1)

    @staticmethod
    def generateFile():
        """
        Generate config file
        """


_CONFIG = ConfigObject()
