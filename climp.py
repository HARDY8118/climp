#!/usr/bin/env python3

import sys
import re

from ClimpImage import ClimpImage
from ClimpConfig import _CONFIG

img = ClimpImage()


def showhelp(command = None):
    print(command)


def parse(cmd):
    if re.match(r'^exit$', cmd, re.IGNORECASE):
        if img.transformation is None:
            sys.exit()
        else:
            if input("Unsaved changes. Quit? [y,n] ").lower()=='y':
                sys.exit()
            else:
                pass
    elif re.match(r'^help\s[a-z]*', cmd, re.IGNORECASE):
        _c = re.findall(r'^help\s([a-z]+)', cmd, re.IGNORECASE)
        if _c:
            showhelp(_c[0])
        else:
            showhelp()
    elif re.match(r'^open\s[a-zA-Z0-9\.\/]*$', cmd):
        _f = (r'^open(\s[a-zA-Z0-9\.\/]*)?$', cmd)
        if img.transformation is not None:
            img.open(_f[0])
        else:
            if input("Unsaved changes. Overwrite? [y,n] ").lower() == 'y':
                img.open(_f[0])
            else:
                pass
    elif re.match(r'^new\s[0-9]+\s[0-9]+$', cmd):
        (_, _width, _height) = re.match(r'^new\s([0-9]+)\s([0-9]+)$', cmd).group().split()
        if img.transformation is None:
            img.new(int(_width), int(_height))
            if _CONFIG.verbose:
                print(f"Created new image [{_width}x{_height}]")
        else:
            if input("Unsaved changes. Overwrite? [y,n] ").lower() == 'y':
                img.new(int(_width), int(_height))
                print(f"Created new image [{_width}x{_height}]")
            else:
                pass
    elif re.match(r'^save$', cmd):
        try:
            img.save()
            print("Saved")
        except Exception():
            pass
    elif re.match(r'^undo$', cmd):
        img.undo()
    elif re.match(r'^redo$', cmd):
        img.redo()
    elif re.match(r'^file$', cmd):
        print("Currently using", img.getFilePath())
    elif re.match(r'^view$', cmd):
        img.view()
    elif re.match(r'^view\-base$', cmd):
        img.view_base()
    elif re.match(r'^print$', cmd):
        img.view_ascii()
    elif re.match(r'^move\s[xy][+-=]\d+$', cmd):
        _args = re.match(r'^move\s[xy][+-=]\d+$', cmd).group().split()[1:]
        img.move(_args)
    elif re.match(r'^line\s\d+\s\d+[\s[0-9A-Za-z]+?]{0,4}$',cmd):
        _args = re.match(r'^line\s\d+\s\d+[\s[0-9A-Za-z]+?]{0,4}$', cmd).group().split()[1:]
        img.addLine(_args)
    elif re.match(r'^rect\s\d+\s\d+[\s[0-9A-Za-z]+?]{0,5}$',cmd):
        _args = re.match(r'^rect\s\d+\s\d+[\s[0-9A-Za-z]+?]{0,5}$', cmd).group().split()[1:]
        img.addRect(_args)
    elif re.match(r'^circle(\s[0-9]+){1,3}(\s\w+\s[0-9]*)?$', cmd):
        _args = re.match('^circle(\s[0-9]+){1,3}(\s\w+\s[0-9]*)?$', cmd).group().split()[1:]
        img.addCircle(_args)
    elif re.match(r'^text\s\S+(\s[a-z]+){0,1}(\s\d+\s\d+)?(\s[a-z]+)(\s[a-z]+)?(\s[\d]+)?$', cmd):
        _args = re.match(r'^text\s\S+(\s[a-z]+){0,1}(\s\d+\s\d+)?(\s[a-z]+)(\s[a-z]+)?(\s[\d]+)?$', cmd).group().split()[1:]
        img.addText(_args)
    elif re.match(r'^edit\s[a-z]+[=][\w]+$', cmd):
        _args = re.match(r'^edit\s[a-z]+[=][\w]+$', cmd).group().split()[1:]
    else:
        print("Unknown command", cmd)

if _CONFIG.file:
    try:
        img.open(_CONFIG.file)
    except FileNotFoundError as e:
        print("File", e, "does not exists")
        sys.exit()

if __name__=='__main__':
    while True:
        parse(input("\n> "))
