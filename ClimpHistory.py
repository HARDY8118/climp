from datetime import datetime
from hashlib import sha1
from pickle import dump, load

# from ClimpConfig import config

from ClimpExceptions import HistoryError


class CommitHeader:
    """
    Header values for commit block
    """

    def __init__(this, _hash, _prev=None):
        """
        Initialize commit block header
        """
        this.__prev = _prev
        this.__hash = _hash
        this.__next = None

    def __repr__(this):
        return f"{this.__hash} {this.__time}"

    def __str__(this):
        return f"{this.__hash} {this.__time}"

    def hash(this):
        """Get hash value"""
        return this.__hash

    def prev(this):
        """Get previous hash value"""
        return this.__prev

    def next(this):
        """Get previous hash value"""
        return this.__next

    def set_next(this, _hash):
        """
        Add next hash

        param _hash (str) Hash of new object
        """
        this.__next = _hash


class CommitMeta:
    """
    Meta values for commit block
    """

    def __init__(this):
        this.commitTime = datetime.now().strftime("%H%M%s")
        # this.editor = config.author


class Commit:
    """
    Commit block
    """

    def __init__(this, _data, _prev=None):
        """
        Create new commit block

        param _data (numpy 3d array) Image data as numpy array
        param _prev (str?) Hash of current commit or None if first commit

        """
        this.header = CommitHeader(sha1(_data.tobytes()).hexdigest(), _prev)
        this.data = _data
        this.meta = CommitMeta()

    def __repr__(this):
        return f"{this.header} : {Commit.meta.editor if Commit.meta.editor!=None else 'Unknown'}"

    def __str__(this):
        return f"{this.header} : {Commit.meta.editor if Commit.meta.editor!=None else 'Unknown'}"

    def hash(this):
        """Get block hash"""
        return this.header.hash()

    def write(this, _dir):
        """
        Write commit block in file system

        param _dir (pathlib.PosixPath) working directory

        raises FileNotFoundError
        """
        if _dir.exists() and _dir.is_dir():
            _commitFile = open(_dir / "objects" / this.hash(), "wb")
            dump(this, _commitFile)
            _commitFile.close()
        else:
            raise FileNotFoundError("Directory " + str(_dir) + " does not exists")

    @staticmethod
    def read(_dir, _hash):
        """
        Read commit block from hash

        param _dir (pathlib.PosixPath) working directory
        _hash (str) Object hash

        raises FileNotFoundError
        """
        if _hash is None:
            raise HistoryError("No hash value")
        else:
            if (_dir / "objects" / _hash).exists():
                _commitFile = open(_dir / "objects" / _hash, "rb")
                _commitData = load(_commitFile)
                _commitFile.close()
                return _commitData
            else:
                raise FileNotFoundError(_hash + " or " + str(_dir) + " not exist")


class ClimpCommitHistory:
    def __init__(this, _dirpath, _filename):
        """
        Initialize new history object

        param _dirpath: (pathlib.PosixPath) Path of directory
        param _filename: (str) Filename of current file relative to path
        """
        this.current = None  # Current block in timeline. Useful for undoing
        this.dir = _dirpath
        _sepIndex = _filename.rfind("/")
        if _sepIndex != -1:
            this.dir = (this.dir / _filename[:_sepIndex]).resolve()
            this.file = "." + _filename[_sepIndex + 1 :]
        else:
            this.file = "." + _filename

        ((this.dir / this.file).resolve()).mkdir(parents=True, exist_ok=True)
        ((this.dir / this.file).resolve() / "objects").mkdir(
            parents=True, exist_ok=True
        )
        ((this.dir / this.file).resolve() / "History").touch(exist_ok=True)

    def add_commit(this, _data):
        """
        Add new commit block

        param _data (numpy 3d array) Image data as numpy array
        """
        _c = Commit(_data, None if this.current is None else this.current.hash())
        if this.current is not None:
            this.current.header.set_next(_c.hash())
            this.current.write(this.dir / this.file)
        this.current = _c
        this.save()

    def rewind(this):
        """
        Undo commit
        """
        try:
            while this.current is not None:
                this.undo()
        except HistoryError:
            pass

    def undo(this):
        if this.current is None:
            raise HistoryError("Already at first")
        else:
            this.current = Commit.read(this.dir / this.file, this.current.header.prev())

    def redo(this):
        if this.current is None:
            raise HistoryError("Already at first")
        else:
            this.current = Commit.read(this.dir / this.file, this.current.header.next())

    def loadCommit(this, _hash):
        """
        Load commit data from commit hash

        param _hash (str) Hash sequence to load from
        """
        this.current = Commit.read(this.dir / this.file, _hash)

    def save(this):
        """
        Save history object
        """
        _historyFile = open(this.dir / this.file / "History", "wb")
        dump(this, _historyFile, protocol=3)
        _historyFile.close()

    @staticmethod
    def loadHistory(_dirpath, _filename):
        """
        Find and return history object for specified file

        param _dirpath: (pathlib.PosixPath) Path of directory
        param _filename: (str) Filename of current file relative to path
        """
        _filename = "." + _filename
        if ((_dirpath / _filename / "History").resolve()).exists():
            try:
                _historyFile = open(_dirpath / _filename / "History", "rb")
                _historyData = load(_historyFile)
                _historyFile.close()
                return _historyData
            except EOFError:
                _historyFile.close()
                return None
        return None
