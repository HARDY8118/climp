# CLIMP

Command line based image editor program on python using openCV.

# Contents
1. [Introduction](#introduction)
2. [Features](#features)
3. [Terminology](#terminology)
4. [Commands](#commands)
5. [Config](#config)
6. [Colors](#colors)
7. [Lines](#lines)
8. [Fonts](#fonts)
9. [Sample](#samples)

---

# Introduction
Climp is a command line based text editor built on python using [OpenCV](https://opencv.org/).

Climp provides basic image editing like adding shapes and text over images.

Changes made are tracked and stored similar to a version control system
and allowing to move backward or forward in time in terms of changes.

---

# Features
- Text based editing
- View images as you edit
- Extensible with python3 and [OpenCV](https://opencv.org/)

---

# Terminology

## Transformation
Any changes (addition/deletion) of property is referred to as a transformation.

Transformation can be many types like *Line*, *Circle* or *Text*.

Changes in transformation properties (like postion, color) do not constitute to new transformation.

## Base
The image on which the transformations are applied.

## History
List of transformations maintained to keep track of changes.

## Commit
Stage in history representing a state.

A commit containes following information
- Header
    - Previous Hash
    - Current Hash
    - Next Hash
- Meta
    - Commit Time

---

# Commands

- [New](#new)
- [Commit](#commit)
- [Open](#open)
- [Save](#save)
- [Undo](#undo)
- [Redo](#redo)
- [File](#file)
- [View](#view)
- [View-Base](#view-base)
- [Move](#move)
- [Line](#line)
- [Rectangle](#rectangle)
- [Circle](#circle)
- [Text](#text)
- [Edit](#edit)

## New
Create new blank image

New image will be created in `/tmp` directory

**Usage**

`new <width> <height>`

## Commit
Confirm transfromation and add to history

**Usage**

`commit`

## Open
Open image (and changes history) for editing

**Usage**

`open <filepath>`

## Save
Save image file.

A prompt will appear confirming name

## Undo
Undo changes to last commit

**Usage**

`undo`

## Redo
Redo changes to last commit

**Usage**

`redo`

## File
Show current file path

**Usage**

`file`

## View
View current changes in preview window.

**Usage**

`view`

## View-Base
View current base image in preview window.

**Usage**

`view-base`

## Move
Change x,y coordinates of last transformation.

**Usage**

`move x=<pos>`

Set x1 to &lt;pos&gt;

---

`move x2=<pos>`

Set x2 to &lt;pos&gt;

---

`move y=<pos>`

Set y1 to &lt;pos&gt;

---

`move y2=<pos>`

Set y2 to &lt;pos&gt;

---

`move x+<pos>`

Increase x1 by &lt;pos&gt; pixels

---

`move y+<pos>`

Increase y1 by &lt;pos&gt; pixels

---

`move x2-<pos>`

Decrease x2 by &lt;pos&gt; pixels

---

`move y2-<pos>`

Decrease y2 by &lt;pos&gt; pixels

---

## Line
Draw line

**Usage**

`line <x> <y>`

Draw line from (0,0) to (&lt;x&gt;,&lt;y&gt;)

---

`line <x> <y> <color>`

Draw line from (0,0) to (&lt;x&gt;,&lt;y&gt;) with color &lt;color&gt;

---

`line <x1> <y1> <x2> <y2>`

Draw line from (&lt;x1&gt;,&lt;y1&gt;) to (&lt;x2&gt;,&lt;y2&gt;)

---

`line <x1> <y1> <x2> <y2> <color>`

Draw line from (&lt;x1&gt;,&lt;y1&gt;) to (&lt;x2&gt;,&lt;y2&gt;) with &lt;color&gt;

---

`line <x1> <y1> <x2> <y2> <color> <thickness>`

Draw line from (&lt;x1&gt;,&lt;y1&gt;) to (&lt;x2&gt;,&lt;y2&gt;) with &lt;color&gt; and &lt;thickness&gt; pixels thick

---

## Rectangle
Draw rectangle

**Usage**

`rect <x> <y>`

Draw rectangle from (0,0) to (&lt;x&gt;,&lt;y&gt;)

---

`rect <x> <y> <color>`

Draw rectangle from (0,0) to (&lt;x&gt;,&lt;y&gt;) with color &lt;color&gt;

---

`rect <x1> <y1> <x2> <y2>`

Draw rectangle from (&lt;x1&gt;,&lt;y1&gt;) to (&lt;x2&gt;,&lt;y2&gt;)

---

`rect <x1> <y1> <x2> <y2> <color>`

Draw rectangle from (&lt;x1&gt;,&lt;y1&gt;) to (&lt;x2&gt;,&lt;y2&gt;) with &lt;color&gt;

---

`rect <x1> <y1> <x2> <y2> <color> <thickness>`

Draw rectangle from (&lt;x1&gt;,&lt;y1&gt;) to (&lt;x2&gt;,&lt;y2&gt;) with &lt;color&gt; and &lt;thickness&gt; pixels thick

---

`rect <x1> <y1> <x2> <y2> <color> <thickness> <linetype>`

Draw rectangle from (&lt;x1&gt;,&lt;y1&gt;) to (&lt;x2&gt;,&lt;y2&gt;) with &lt;color&gt; and &lt;thickness&gt; pixels thick using &lt;linetype&gt;

---

## Circle
Draw circle

**Usage**

`circle <radius>`

Draw circle of radius &lt;radius&gt; pixels with centre (0,0)

---

`circle <x> <y>`

Draw circle of radius 1 pixels with centre (&lt;x&gt;,&lt;y&gt;)

---

`circle <x> <y> <radius> `

Draw circle of radius &lt;radius&gt; pixels with centre (&lt;x&gt;,&lt;y&gt;)

---

`circle <x> <y> <radius> <color>`

Draw circle of radius &lt;radius&gt; pixels with centre (&lt;x&gt;,&lt;y&gt;) of with &lt;color&gt;

---

`circle <x> <y> <radius> <color> <thickness>`

Draw circle of radius &lt;radius&gt; pixels with centre (&lt;x&gt;,&lt;y&gt;) of with color &lt;color&gt; and thickness &lt;thickness&gt;

---

## Text
Draw text

**Usage**

`text <text>`

Draw text &lt;text&gt; at (0,0)

---

`text <text> <color>`

Draw text &lt;text&gt; at (0,0) with color &lt;color&gt;

---

`text <text> <x> <y>`

Draw text &lt;text&gt; at (&lt;x&gt;,&lt;y&gt;)

---

`text <text> <x> <y> <color>`

Draw text &lt;text&gt; at (&lt;x&gt;,&lt;y&gt;) with color &lt;color&gt;

---

`text <text> <x> <y> <color> <font>`

Draw text &lt;text&gt; at (&lt;x&gt;,&lt;y&gt;) with color &lt;color&gt; and font &lt;font&gt;

---

`text <text> <x> <y> <color> <font> <size>`

Draw text &lt;text&gt; of size &lt;size&gt; at (&lt;x&gt;,&lt;y&gt;) with color &lt;color&gt; and font &lt;font&gt;

---

`text <text> <x> <y> <color> <font> <size> <thickness>`

Draw text &lt;text&gt; of size &lt;size&gt; and thickness &lt;thickness&gt; at (&lt;x&gt;,&lt;y&gt;) with color &lt;color&gt; and font &lt;font&gt;

---

`text <text> <x> <y> <color> <font> <size> <thickness> <linetype>`

Draw text &lt;text&gt; of size &lt;size&gt; and thickness &lt;thickness&gt; with line type &lt;linetype&gt; at (&lt;x&gt;,&lt;y&gt;) with color &lt;color&gt; and font &lt;font&gt;

---

## Edit
Edit **any** property of last transformation

**Usage**

`edit property=<new value>`

**Example**

`edit color=red`

Change color of last transformation to &lt;color&gt;

---

# Colors
Colors can be provided as their hex value.

In addition some common colors are present as preset which can be referenced with their names (in lowercase)

## Presets
- black &nbsp; <font color="#000000">██</font>
- blue &nbsp; <font color="#0000ff">██</font>
- cyan &nbsp; <font color="#00ffff">██</font>
- green &nbsp; <font color="#00ff00">██</font>
- grey &nbsp; <font color="#808080">██</font>
- red &nbsp; <font color="#ff0000">██</font>
- pink &nbsp; <font color="#ff00ff">██</font>
- white &nbsp; <font color="#ffffff">██</font>
- yellow &nbsp; <font color="#ffff00">██</font>

Default color is **black** if unspecified in any command.

---

# Lines
Climp uses [OpenCV](https://opencv.org/)'s prebuilt line types.

Available options are

| Option | OpenCV Line Type |
|---|---|
| **-1** or **FILLED** or **FILL** | FILLED |
| **4** or **LINE_4** | LINE_4 |
| **8** or **LINE_8** | LINE_8 |
| **16** or **AA** or **LINE_16** or **LINE_AA** | LINE_AA |

Default line type is **-1**.

---

# Fonts
Climp uses [OpenCV](https://opencv.org/)'s pre-available fonts.

| Font Name | OpenCV Font |
|---|---|
| FONT_HERSHEY_SIMPLEX | FONT_HERSHEY_SIMPLEX |
| HERSHEY_SIMPLEX | HERSHEY_SIMPLEX |
| SIMPLEX | SIMPLEX |
| FONT_HERSHEY_PLAIN | FONT_HERSHEY_PLAIN |
| HERSHEY_PLAIN | HERSHEY_PLAIN |
| PLAIN | PLAIN |
| FONT_HERSHEY_DUPLEX | FONT_HERSHEY_DUPLEX |
| HERSHEY_DUPLEX | HERSHEY_DUPLEX |
| DUPLEX | DUPLEX |
| FONT_HERSHEY_COMPLEX | FONT_HERSHEY_COMPLEX |
| HERSHEY_COMPLEX | HERSHEY_COMPLEX |
| COMPLEX | COMPLEX |
| FONT_HERSHEY_TRIPLEX | FONT_HERSHEY_TRIPLEX |
| HERSHEY_TRIPLEX | HERSHEY_TRIPLEX |
| TRIPLEX | TRIPLEX |
| FONT_HERSHEY_COMPLEX_SMALL | FONT_HERSHEY_COMPLEX_SMALL |
| HERSHEY_COMPLEX_SMALL | HERSHEY_COMPLEX_SMALL |
| COMPLEX_SMALL | COMPLEX_SMALL |
| FONT_HERSHEY_SCRIPT_SIMPLEX | FONT_HERSHEY_SCRIPT_SIMPLEX |
| HERSHEY_SCRIPT_SIMPLEX | HERSHEY_SCRIPT_SIMPLEX |
| SCRIPT_SIMPLEX | SCRIPT_SIMPLEX |
| FONT_HERSHEY_SCRIPT_COMPLEX | FONT_HERSHEY_SCRIPT_COMPLEX |
| HERSHEY_SCRIPT_COMPLEX | HERSHEY_SCRIPT_COMPLEX |
| SCRIPT_COMPLEX | SCRIPT_COMPLEX |

Default font is **plain**

---

# Samples
