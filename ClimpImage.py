from numpy import ndarray
from pathlib import Path
from datetime import datetime
import cv2
from copy import deepcopy
from os import get_terminal_size

from ClimpHistory import ClimpCommitHistory
from ClimpTransform import (
    parse_changes,
    LineTransform,
    RectTransform,
    CircleTransform,
    TextTransform,
)

from ClimpExceptions import UnsavedFileError, InvalidArguments, HistoryError


class ClimpImage:
    def __init__(this):
        this.base = None  # Before transformation
        this.transformation = None  # Transformation properties
        this.final = None  # After transformation

    def new(this, _width, _height):
        """
        Create new blank image by initializing numpy array

        param _height Height of image
        param _width Width of image
        """
        if this.transformation is not None or this.final is not None:
            raise UnsavedFileError()
        this.base = ndarray([_height, _width, 3], dtype="uint8")
        this.path = (
            Path("/tmp"),
            "Untitled" + datetime.now().strftime("%H%M%s") + ".png",
        )
        this.history = ClimpCommitHistory(this.path[0], this.path[1])

    def transform(this):
        """
        Apply transformation to image
        """
        if this.transformation is not None:
            this.final = deepcopy(this.base)
            this.transformation.apply(this.final)
        else:
            print("Nothing to apply")

    def commit(this):
        if this.final is not None:
            this.history.add_commit(this.final)
            this.base = this.final
            this.transformation = None
            this.final = None
        else:
            print("Nothing to commit")

    def open(this, _file):
        if this.transformation is not None or this.final is not None:
            raise UnsavedFileError()
        this.history = ClimpCommitHistory.loadHistory(Path.cwd(), _file)
        if this.history is None:
            print("Previous history not found")
            this.history = ClimpCommitHistory(Path.cwd(), _file)
        this.path = (Path.cwd(), _file)
        if (Path.cwd() / _file).exists():
            this.base = cv2.imread(str(Path.cwd() / _file))
        else:
            raise FileNotFoundError(str(Path.cwd() / _file))

    def save(this):
        if this.final is not None:
            print("Saving as " + str(this.path[0] / this.path[1]))
            _inputFileName = input("Type new path or press [ENTER]: ")
            if _inputFileName == "":
                cv2.imwrite(str(this.path[0] / this.path[1]), this.final)
                print("Saved as " + str(this.path[0] / this.path[1]))
            else:
                _sep = _inputFileName.rfind("/")
                if _sep == -1:
                    print("Invalid name. Retry")
                else:
                    this.path = (
                        Path(_inputFileName[:_sep]),
                        _inputFileName[(_sep + 1) :],
                    )
                    this.path[0].mkdir(parents=True, exist_ok=True)
                    cv2.imwrite(str(this.path[0] / this.path[1]), this.final)
                    print("Saved as " + str(this.path[0] / this.path[1]))
        elif this.base is not None:
            print("Saving as " + str(this.path[0] / this.path[1]) + ".png")
            _inputFileName = input("Type new full path or press [ENTER]: ")
            if _inputFileName == "":
                cv2.imwrite(str(this.path[0] / this.path[1]), this.base)
                print("Saved as " + str(this.path[0] / this.path[1]))
            else:
                _sep = _inputFileName.rfind("/")
                if _sep == -1:
                    print("Invalid name. Retry")
                else:
                    this.path = (
                        Path(_inputFileName[:_sep]),
                        _inputFileName[(_sep + 1) :],
                    )
                    this.path[0].mkdir(parents=True, exist_ok=True)
                    cv2.imwrite(str(this.path[0] / this.path[1]), this.base)
                    print("Saved as " + str(this.path[0] / this.path[1]))
        else:
            print("No data to save")

    def undo(this):
        try:
            this.history.undo()
            this.base = this.history.current.data
        except HistoryError as e:
            print(e)

    def redo(this):
        try:
            this.history.redo()
            this.base = this.history.current.data
        except HistoryError as e:
            print(e)

    def getFilePath(this):
        return str(this.path[0] / this.path[1])

    def view(this):
        """
        View image in window
        """
        if this.final is not None:
            cv2.imshow(this.getFilePath(), this.final)
            cv2.waitKey(0)
        # elif this.base is not None:
        #     cv2.imshow(this.getFilePath(), this.base)
        #     cv2.waitKey(0)
        else:
            print("No image data")

    def view_base(this):
        """
        View base image
        """
        if this.base is not None:
            cv2.imshow(this.getFilePath(), this.base)
            cv2.waitKey(0)
        else:
            print("No image data")

    def view_ascii(this):
        if this.final is not None:
            if get_terminal_size().lines < len(this.final) or (
                get_terminal_size().columns // 2
            ) < len(this.final[0]):
                print("Image too large for terminal")
            else:
                for _r in this.final:
                    for _c in _r:
                        print(
                            "\033[38;2;{};{};{}m{} \033[38;2;255;255;255m".format(
                                _c[2], _c[1], _c[0], "▅"
                            ),
                            end="",
                        )
                    print()
        else:
            print("No image data")

    def view_ascii_base(this):
        if this.base is not None:
            if get_terminal_size().lines < len(this.base) or (
                get_terminal_size().columns // 2
            ) < len(this.base[0]):
                print("Image too large for terminal")
            else:
                for _r in this.base:
                    for _c in _r:
                        print(
                            "\033[38;2;{};{};{}m{} \033[38;2;255;255;255m".format(
                                _c[2], _c[1], _c[0], "▅"
                            ),
                            end="",
                        )
                    print()
        else:
            print("No image data")

    def move(this, args):
        this.transformation.move(args)
        this.transform()

    def addLine(this, args):
        """
        Create line transformation object
        """
        if len(args) == 0:
            return
        elif len(args) == 1:
            raise InvalidArguments(args)
        elif len(args) == 2:  # x2 y2
            this.transformation = LineTransform(0, 0, x2=args[0], y2=args[1])
        elif len(args) == 3:  # x2 y2 color
            this.transformation = LineTransform(
                0, 0, x2=args[0], y2=args[1], color=args[2]
            )
        elif len(args) == 4:  # x y x2 y2
            this.transformation = LineTransform(args[0], args[1], args[2], args[3])
        elif len(args) == 5:  # x y x2 y2 color
            this.transformation = LineTransform(
                args[0], args[1], args[2], args[3], args[4]
            )
        elif len(args) == 6:  # x y x2 y2 color thickness
            this.transformation = LineTransform(
                args[0], args[1], args[2], args[3], args[4], args[5]
            )
        else:
            raise InvalidArguments(args)

        if this.transformation is not None:
            this.transform()

    def addRect(this, args):
        """
        Create rectangle transformation object
        """
        if len(args) == 0:
            return
        elif len(args) == 1:
            raise InvalidArguments(args)
        elif len(args) == 2:  # x2 y2
            this.transformation = RectTransform(0, 0, x2=args[0], y2=args[1])
        elif len(args) == 3:  # x2 y2 color
            this.transformation = RectTransform(
                0, 0, x2=args[0], y2=args[1], color=args[2]
            )
        elif len(args) == 4:  # x y x2 y2
            this.transformation = RectTransform(args[0], args[1], args[2], args[3])
        elif len(args) == 5:  # x y x2 y2 color
            this.transformation = RectTransform(
                args[0], args[1], args[2], args[3], args[4]
            )
        elif len(args) == 6:  # x y x2 y2 color thickness
            this.transformation = RectTransform(
                args[0], args[1], args[2], args[3], args[4], args[5]
            )
        elif len(args) == 7:  # x y x2 y2 color thickness linetype
            this.transformation = RectTransform(
                args[0], args[1], args[2], args[3], args[4], args[5], args[6]
            )
        else:
            raise InvalidArguments(args)

        if this.transformation is not None:
            this.transform()

    def addCircle(this, args):
        """
        Create circle transformation object
        """
        if len(args) == 0:
            return
        elif len(args) == 1:  # radius
            this.transformation = CircleTransform(0, 0, args[0])
        elif len(args) == 2:  # x y
            this.transformation = CircleTransform(args[0], args[1])
        elif len(args) == 3:  # x y radius
            this.transformation = CircleTransform(args[0], args[1], args[2])
        elif len(args) == 4:  # x y radius color
            this.transformation = CircleTransform(args[0], args[1], args[2], args[3])
        elif len(args) == 5:  # x y radius color thickness
            this.transformation = CircleTransform(
                args[0], args[1], args[2], args[3], args[4]
            )
        else:
            raise InvalidArguments(args)

        if this.transformation is not None:
            this.transform()

    def addText(this, args):
        """
        Create text transformation object
        """
        if len(args) == 0:
            return
        elif len(args) == 1:  # Text
            this.transformation = TextTransform(args[0])
        elif len(args) == 2:  # Text Color
            this.transformation = TextTransform(args[0], color=args[1])
        elif len(args) == 3:  # Text X Y
            this.transformation = TextTransform(args[0], args[1], args[2])
        elif len(args) == 4:  # Text X Y Color
            this.transformation = TextTransform(
                args[0], args[1], args[2], color=args[3]
            )
        elif len(args) == 5:  # Text X Y Color Font
            this.transformation = TextTransform(
                args[0], args[1], args[2], args[4], color=args[3]
            )
        elif len(args) == 6:  # Text X Y Color Font Size
            this.transformation = TextTransform(
                args[0],
                args[1],
                args[2],
                args[4],
                args[5],
                color=args[3],
            )
        elif len(args) == 7:  # Text X Y Color Font Size Thickness
            this.transformation = TextTransform(
                args[0],
                args[1],
                args[2],
                args[4],
                args[5],
                args[6],
                color=args[3],
            )
        elif len(args) == 8:  # Text X Y Color Font Size Thickness Line
            this.transformation = TextTransform(
                args[0],
                args[1],
                args[2],
                args[4],
                args[5],
                args[6],
                args[7],
                color=args[3],
            )
        else:
            raise InvalidArguments(args)

        if this.transformation is not None:
            this.transform()

    def edit(this, args):
        """
        Edit transformation properties

        param args (str) property=new_value
        """
        if this.transformation is not None:
            this.transformation.modify(parse_changes(args))
            this.transform()
        else:
            print("No transformation")
